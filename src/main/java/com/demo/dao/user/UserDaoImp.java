package com.demo.dao.user;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.demo.application.entities.User;
import com.demo.custommapper.UserMapper;

@Repository
public class UserDaoImp implements UserDao{

	@Autowired
	private UserMapper usermapper;

	@Override
	public List<User> findAll() {
		return (List<User>) usermapper.findAll();
	}

	@Override
	public void saveUser(User user) {
		usermapper.saveUser(user);
	}

	@Override
	public void updateUser(User user) {
		usermapper.updateUser(user);
	}

	@Override
	public void deleteUser(Long userid) {
		usermapper.deleteUser(userid);
	}
 
}
