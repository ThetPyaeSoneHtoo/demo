package com.demo.zconfig.dto;

import java.util.Date;
import lombok.Data;

@Data
public class Result {
	private String status;
	private Object data;
	private Date serverTime;
}
