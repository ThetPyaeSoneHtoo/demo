package com.demo.dao.user;

import java.util.List;

import com.demo.application.entities.User;

public interface UserDao {
	
	public List<User> findAll();

	public void saveUser(User user);

	public void updateUser(User user);
	
	public void deleteUser(Long userid);

}
