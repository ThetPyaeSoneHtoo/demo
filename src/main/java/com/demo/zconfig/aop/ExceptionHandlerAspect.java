package com.demo.zconfig.aop;

import java.sql.SQLException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import com.demo.zconfig.MessageCode;
import com.demo.zconfig.exception.DAOException;

@Aspect
@Component
public class ExceptionHandlerAspect extends PointCutConfig {
	private Logger logger = LogManager.getLogger(ExceptionHandlerAspect.class);

	@AfterThrowing(pointcut = "publicMethodInsideDaoBean()", throwing = "e")
	public void daoThrowing(JoinPoint joinPoint, Exception e) throws Throwable {
		logger.error("Unexpected error in dao", e);
		DAOException dae = null;
		Throwable throwable = e;
		while (throwable != null && !(throwable instanceof SQLException)) {
			throwable = throwable.getCause();
		}
		if (throwable instanceof SQLException) {
			dae = new DAOException(MessageCode.DB_PROCESS_ERROR, "Unexpected error in dao", (SQLException) throwable);
		}
		if (dae != null) {
			throw dae;
		} else {
			throw new DAOException(MessageCode.DB_PROCESS_ERROR, "Unexpected error in dao", e);
		}
	}

	@AfterThrowing(pointcut = "publicMethodInsideMapper()", throwing = "e")
	public void mapperThrowing(JoinPoint joinPoint, Exception e) throws Throwable {
		DAOException dae = null;
		Throwable throwable = e;
		while (throwable != null && !(throwable instanceof SQLException)) {
			throwable = throwable.getCause();
		}
		if (throwable instanceof SQLException) {
			dae = new DAOException(MessageCode.DB_PROCESS_ERROR, "Unexpected error in mapper.", (SQLException) throwable);
		}
		if (dae != null) {
			throw dae;
		} else {
			throw new DAOException(MessageCode.DB_PROCESS_ERROR, "Unexpected error in mapper", e);
		}
	}
	
	@AfterThrowing(pointcut = "publicMethodInsideServiceBean()", throwing = "e")
	public void serviceThrowing(JoinPoint joinPoint, Exception e) throws Throwable {
		MethodSignature signature = (MethodSignature) joinPoint.getSignature();
		String errorMessage = e + " occur in line no " + e.getStackTrace()[0].getLineNumber() + " of " + signature.getMethod().getDeclaringClass() + "." + signature.getName() + " with parameter {";
		for (int i = 0; i < joinPoint.getArgs().length; i++) {
			errorMessage += joinPoint.getArgs()[i];
			if (i < joinPoint.getArgs().length - 1) {
				errorMessage += " , ";
			}
		}
		errorMessage += "}";
		logger.error(errorMessage);
		e.printStackTrace();
	}
	
	@AfterThrowing(pointcut = "publicMethodInsideController()", throwing = "e")
	public void controllerThrowing(JoinPoint joinPoint, Exception e) throws Throwable {
		MethodSignature signature = (MethodSignature) joinPoint.getSignature();
		String errorMessage = e + " occur in " + signature.getMethod().getDeclaringClass() + "." + signature.getName()+ " with parameter {";
		for (int i = 0; i < joinPoint.getArgs().length; i++) {
			errorMessage += joinPoint.getArgs()[i];
			if (i < joinPoint.getArgs().length - 1) {
				errorMessage += " , ";
			}
		}
		errorMessage += "}";
		logger.error(errorMessage);
		e.printStackTrace();
	}

}