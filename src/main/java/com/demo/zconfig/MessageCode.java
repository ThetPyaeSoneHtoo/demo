package com.demo.zconfig;

public class MessageCode {
	public static final String UNEXPECTED_ERROR = "UNEXPECTED_ERROR";
	public static final String DB_PROCESS_ERROR = "DB_PROCESS_ERROR";
	public static final String INVALID_REQUEST_PARAMETER = "INVALID_REQUEST_PARAMETER";
}
