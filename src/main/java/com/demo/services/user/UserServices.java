package com.demo.services.user;

import java.util.List;

import com.demo.application.entities.*;


public interface UserServices {
	
	public List<User> findAll();

	public void saveUser(User user);

	public void updateUser(User user);
	
	public void deleteUser(Long userid);

}
