package com.demo.zconfig.aop;

import java.time.LocalDateTime;
import java.util.Date;

import org.springframework.core.MethodParameter;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import com.demo.zconfig.MessageCode;
import com.demo.zconfig.dto.ApiError;
import com.demo.zconfig.dto.ApiStatus;
import com.demo.zconfig.dto.Result;

@ControllerAdvice
public class ResponseEntityHandler implements ResponseBodyAdvice<Object>{

	@Override
	public boolean supports(MethodParameter returnType, Class<? extends HttpMessageConverter<?>> converterType) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public Object beforeBodyWrite(Object body, MethodParameter returnType, MediaType selectedContentType,
			Class<? extends HttpMessageConverter<?>> selectedConverterType, ServerHttpRequest request,
			ServerHttpResponse response) {
		response.setStatusCode(HttpStatus.OK);
		if (body instanceof RuntimeException) {
			RuntimeException re = (RuntimeException) body;
			ApiError apiError = new ApiError(ApiStatus.FAILED);
			apiError.setMessage(re.getMessage());
			apiError.setMessageCode(MessageCode.UNEXPECTED_ERROR);
			apiError.setTimestamp(LocalDateTime.now());
			return apiError;
		}
		if (body instanceof ApiError) {
			return body;
		}
		if (body instanceof String) {
			return body;
		}
		if (returnType.getParameterType().isAssignableFrom(void.class)) {
			Result result = new Result();
			result.setServerTime(new Date());
			result.setData(body);
			result.setStatus(ApiStatus.SUCCESS);
			return result;
		}
		Result result = new Result();
		result.setServerTime(new Date());
		result.setData(body);
		result.setStatus(ApiStatus.SUCCESS);
		return result;
	}

}
