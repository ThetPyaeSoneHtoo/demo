package com.demo.application.controllers;

import com.demo.application.entities.User;
import com.demo.services.user.UserServices;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class UserController {

	@Autowired
	private UserServices userServices; 

	@GetMapping(value = "/users")
	public List<User> getUsers() {
		return (List<User>)userServices.findAll();
	}

	@PostMapping("/users")
	void addUser(@RequestBody User user) {
		userServices.saveUser(user);
	}
	
	@PostMapping("/updateuser")
	void updateUser(@RequestBody User user) {
		userServices.updateUser(user);
	}
	
	@PostMapping("/deleteuser")
	void deleteUser(@RequestBody Long userid) {
		userServices.deleteUser(userid);
	}

}
