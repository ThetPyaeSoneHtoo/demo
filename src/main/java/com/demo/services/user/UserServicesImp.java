package com.demo.services.user;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.application.entities.User;
import com.demo.dao.user.*;
import com.demo.services.user.UserServices;
import com.demo.zconfig.oauth2.*;

@Service
public class UserServicesImp implements  UserServices{
	
	@Autowired
	private CustomPasswordEncoder passwordEncoder;
	
	@Autowired
	private UserDao userdao;

	@Override
	public List<User> findAll() {
		return (List<User>) userdao.findAll();
	}

	@Override
	public void saveUser(User user) {
		String encryptedPassword=passwordEncoder.encode(user.getPassword());
		user.setPassword(encryptedPassword);
		userdao.saveUser(user);
	}

	@Override
	public void updateUser(User user) {
		userdao.updateUser(user);
	}

	@Override
	public void deleteUser(Long userid) {
		userdao.deleteUser(userid);
	}


}
