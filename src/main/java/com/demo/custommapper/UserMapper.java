package com.demo.custommapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.demo.application.entities.*;

public interface UserMapper {

	public List<User> findAll();

	public void saveUser(@Param("user") User user);

	public void updateUser(@Param("user") User user);
	
	public void deleteUser(@Param("userid") Long userid);
	
	

}
