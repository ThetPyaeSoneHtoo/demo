package com.demo.zconfig.aop;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Aspect
@Component
public class LoggingAspect extends PointCutConfig{
	
	private Logger logger = LogManager.getLogger(LoggingAspect.class);
	
	@Autowired
	private HttpServletRequest request;
	
	@Before("publicMethodInsideServiceBean() || publicMethodInsideDaoBean() || publicMethodInsideController()")
	public void beforeServiceDao(JoinPoint joinPoint) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			String logMessage = String.format("%s.%s() method has been started.",
					joinPoint.getSignature().getDeclaringType(), joinPoint.getSignature().getName());
			String parameter = mapper.writeValueAsString(joinPoint.getArgs());
			addRemoteIpAddress();
			logger.debug(logMessage + " Parameter => " + parameter);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}
	
	@AfterReturning(pointcut = "publicMethodInsideServiceBean() || publicMethodInsideDaoBean() || publicMethodInsideController()", returning = "result")
	public void afterReturningServiceDao(JoinPoint joinPoint, Object result) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			String logMessage = String.format("%s.%s() method has been finished.",
					joinPoint.getSignature().getDeclaringType(), joinPoint.getSignature().getName());
			String returnResult = mapper.writeValueAsString(result);
			addRemoteIpAddress();
			logger.debug(logMessage + " Return Result => " + returnResult);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}
	
	public void addRemoteIpAddress() {	
		MDC.putCloseable("ip", request.getRemoteAddr());
	}


}
