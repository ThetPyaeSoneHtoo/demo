package com.demo.dto.user;

public class UserDto {

	private long id;
	private String name;
	private String password;
	private String confirmpassword;
	private String email;
	private String address;
	private int phoneno;

	public UserDto() {

	}

	public UserDto(long id, String name, String password, String confirmpassword, String email, String address,
			int phoneno) {
		super();
		this.id = id;
		this.name = name;
		this.password = password;
		this.confirmpassword = confirmpassword;
		this.email = email;
		this.address = address;
		this.phoneno = phoneno;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getPhoneno() {
		return phoneno;
	}

	public void setPhoneno(int phoneno) {
		this.phoneno = phoneno;
	}

	public String getConfirmpassword() {
		return confirmpassword;
	}

	public void setConfirmpassword(String confirmpassword) {
		this.confirmpassword = confirmpassword;
	}

}
